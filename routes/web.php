<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');

});

/** ------------------------------------------ SGC - SOLICITUD ----------------------------------------- */
/**
 * Para el reporte de ventas
 */

/** -------- Reporte de ventas -------- */
Route::get('ventas/reporte/reporteVentas', [
    'uses' => 'ventas\VentaController@reporteVentas',
    'as' => 'ventas.reporte.reporteVentas'
]);
