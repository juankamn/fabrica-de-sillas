<?php

namespace App\src\ventas;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de la tabla de ventas
 * Class Venta
 * @package App\src\ventas\venta\Venta
 */
class Venta extends Model
{
    protected $table = 'venta';
    protected $fillable = ['vendedor_id','fecha_venta'];

    public function scopeSearch($query,$buscar)
    {
        return $query->where('nombre','LIKE',"%$buscar%");
    }

    /**
     * Una venta tiene solo un vendedor
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Vendedor()
    {
        return $this->hasOne('App\ventas\configuracion\Vendedor', 'id', 'users_id');
    }


    /**
     * Una venta tiene uno o varios detalles de venta
     * relación 1:n
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function DetalleVenta()
    {
        return $this->hasMany('App\src\ventas\venta\DetalleVenta', 'pedido_id', 'id');
    }
}
