<?php

namespace App\src\ventas\venta;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de la tabla de detalles de venta
 * Class DetalleVenta
 * @package App\src\ventas\venta\DetalleVenta
 */
class DetalleVenta extends Model
{
    protected $table = 'detalle_venta';
    protected $fillable = ['venta_id', 'producto_id', 'cantidad', 'precio'];

    public function scopeSearch($query,$buscar)
    {
        return $query->where('nombre','LIKE',"%$buscar%");
    }

    /**
     * Un detalle de venta tiene solo un producto
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Producto()
    {
        return $this->hasOne('App\ventas\configuracion\Producto', 'id', 'producto_id');
    }

    /**
     * Un detalle de venta pertenece solo a una venta
     * relación 1:1
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Venta()
    {
        return $this->hasOne('App\ventas\venta\Venta', 'id', 'venta_id');
    }
}
