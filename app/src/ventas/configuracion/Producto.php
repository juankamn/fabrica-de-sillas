<?php

namespace App\src\ventas\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de la tabla de productos
 * Class Producto
 * @package App\src\ventas\configuracion\producto
 */
class Producto extends Model
{

    protected $table = 'producto';
    protected $fillable = ['nombre', 'precio', 'eliminado'];


    public function scopeSearch($query,$buscar)
    {
        return $query->where('nombre','LIKE',"%$buscar%");
    }

}
