<?php

namespace App\src\ventas\configuracion;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo para el manejo de la tabla de vendedores
 * Class Vendedor
 * @package App\src\ventas\configuracion\vendedor
 */
class Vendedor extends Model
{
    protected $table = 'vendedor';
    protected $fillable = ['nombre', 'eliminado'];

    public function scopeSearch($query,$buscar)
    {
        return $query->where('nombre','LIKE',"%$buscar%");
    }
}
