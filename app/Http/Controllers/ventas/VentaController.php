<?php

namespace App\Http\Controllers\ventas;

use App\Http\Controllers\Controller;
use App\src\ventas\configuracion\Producto;
use App\src\ventas\configuracion\Vendedor;
use App\src\ventas\venta\DetalleVenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentaController extends Controller
{
    /**
     * Función que retorna una vista con los resultados del reporte general de ventas
     * Requerimiento 38
     * @param Request $request
     * @return mixed
     */
    public function reporteVentas()
    {
        $detalleSillasVendidas = DetalleVenta::select(DB::raw('SUM(detalle_venta.cantidad) as total, producto_id'))->groupBy('producto_id')->get();

        $productos = Producto::all();

        $detalleCantidadSillasVendedor = DetalleVenta::select(DB::raw('SUM(detalle_venta.cantidad) as total, producto_id'))
            ->join('venta', 'detalle_venta.venta_id', '=', 'venta.id')
            ->groupBy('venta.vendedor_id')
            ->groupBy('producto_id')->get();

        dd($detalleCantidadSillasVendedor);


        $detalleVentasVendedor =DetalleVenta::select(DB::raw('SUM(detalle_venta.cantidad*detalle_venta.precio) as total, venta.vendedor_id'))
            ->join('venta', 'detalle_venta.venta_id', '=', 'venta.id')
            ->join('vendedor', 'venta.vendedor_id', '=', 'vendedor.id')
            ->groupBy('venta.vendedor_id')->get();

        $vendedores = Vendedor::all();




        return view('ventas.reporte.reporte_ventas')->with('detalleSillasVendidas',$detalleSillasVendidas)
            ->with('productos',$productos)
            ->with('vendedores',$vendedores)
            ->with('detalleVentasVendedor', $detalleVentasVendedor);
    }
}
