<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
          integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>
</head>

<body>

<div class="panel panel-primary">
    <div class="panel-heading text-center text-bold">
        Reportes
    </div>
    <div class="panel-body">
        <div class="form-group">

            <div class="panel panel-info">
                <div class="panel-heading text-center text-bold">
                    Cantidad total de sillas vendidas
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <table class="table table-bordered">
                            <th class="col-xs-6">Nombre Silla</th>
                            <th class="col-xs-6">Cantidad de ventas</th>
                            @foreach($productos as $producto)
                                <tr>
                                    <td>{{$producto->nombre}}</td>
                                    <td>{{$detalleSillasVendidas->where('producto_id',$producto->id)->first()->total}}</td>
                                </tr>
                            @endforeach

                        </table>

                    </div>
                </div>
            </div>














            <div class="panel panel-info">
                <div class="panel-heading text-center text-bold">
                    Monto total vendido por cada ejecutivo comercial
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <table class="table table-bordered">
                            <th class="col-xs-6">Vendedor</th>
                            <th class="col-xs-6">Ventas</th>
                            @foreach($vendedores as $vendedor)
                                <tr>
                                    <td>{{$vendedor->nombre}}</td>
                                    <td>$ {{number_format($detalleVentasVendedor->where('vendedor_id',$vendedor->id)->first()->total,0,',','.')}}</td>
                                </tr>
                            @endforeach

                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>
